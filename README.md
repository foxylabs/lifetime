## Usage

##### 1. Create a new private git repository

```bash
mkdir mylifetime
cd mylifetime
git init .
```

##### 2. Add lifetime repository as a submodule

```bash
git submodule add git@gitlab.com:foxylabs/lifetime.git
```

##### 3. (Optional) Copy sample.json for editing

```bash
cp lifetime/sample.json data.json
```

##### 4. View `lifetime/index.html` in browser

If you have your own `data.json` it will be shown, otherwise `sample.json` will be shown.
