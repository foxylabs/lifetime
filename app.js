"use strict";
(function () {
	const DATA_FILENAME = '../data.json'; // This data file will be used by default
	const SAMPLE_FILENAME = 'sample.json'; // Will be shown if default file 404s
	const DATE_FORMAT = {
		year: 'numeric',
		month: 'long',
		day: 'numeric'
	};

	/** Return value is passed to HTML template **/
	function transformDataForTemplate(data) {
		if (location.search && location.search.indexOf('public') > 0) {
			data.events = data.events.filter(function(it){ return !it.private });
		}
		for (let event of data.events) {
			event.date = parseDateISOString(event.date);
		}
		data.events.sort(function(a, b) {
			// Reverse chronological sort
			return b.date.getTime() - a.date.getTime();
		});
		for (let event of data.events) {
			event.type = event.type || 'generic';
			event.date = event.date.toLocaleDateString('en-US', DATE_FORMAT);
			event.dateTitle = typeof(event.approximate) == 'string' && event.approximate;
		}
		return data;
	}


	function parseDateISOString(s) {
		let ds = s.split(/\D/).map(s => parseInt(s));
		ds[1] = ds[1] - 1; // adjust month
		return new Date(...ds);
	}

	function fetchData(filename) {
		return fetch(filename)
			.then(function (response) {
				if (response.status >= 200 && response.status < 300) {
					return response.json();
				}
				if (response.status == 404 && filename == DATA_FILENAME) {
					return fetchData(SAMPLE_FILENAME);
				}
				throw new Error(response.statusText || response.status);
			});
	}

	/** Fetches data and renders it into template **/
	function fetchAndRender(filename) {
		fetchData(filename)
			.then(function (json) {
				var app = new Vue({
					el: '#app',
					data: transformDataForTemplate(json)
				});
				document.getElementsByTagName('body')[0].className += ' loaded';
			})
			.catch(function (err) {
				alert(err.message);
			});
	}
	document.addEventListener("DOMContentLoaded", function() {
		fetchAndRender(DATA_FILENAME);
	});
})();